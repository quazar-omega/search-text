let text = document.getElementById("text");
let search = document.getElementById("search");

let caseSensitive = document.getElementById("caseSensitive");
let wholeWords = document.getElementById("wholeWords");
let invio = document.getElementById("invio");

let risultato = document.getElementById("risultato");
let instance = new Mark(text);


function eseguiRicerca(termineRicerca, caseSensitiveAlleMaiuscole, wholeWords) {
	// reimposta evidenziatore e risultato
	instance.unmark();
	risultato.innerText = "";

	if (termineRicerca != "") {
		/* analizza stringa

		let parole = text.innerText.replace(/[.]/g, '').toLowerCase().split(/\s/);
		console.table(parole);
		*/

		let regex = new String(termineRicerca);
		let modificatori = "ug";
		if (caseSensitiveAlleMaiuscole) {
			modificatori += "i";
		}
		if (wholeWords) {
			// \b non funziona con unicode quindi è necessario utilizzare questo
			// riferimento: https://github.com/slevithan/xregexp/issues/228#issuecomment-811478498
			regex = `(?:(?<=\\p{L}\\p{M}*)(?!\\p{L}\\p{M}*)|(?<!\\p{L}\\p{M}*)(?=\\p{L}\\p{M}*))${regex}(?:(?<=\\p{L}\\p{M}*)(?!\\p{L}\\p{M}*)|(?<!\\p{L}\\p{M}*)(?=\\p{L}\\p{M}*))`;
		}
		let pattern = new RegExp(regex, modificatori);

		console.log(
			`
			termine di ricerca: ${termineRicerca}
			caseSensitive alle maiuscole: ${caseSensitiveAlleMaiuscole}
			parole intere: ${wholeWords}

			regex: /${regex}/${modificatori}
			`
		);
		let istanze = 0;
		try {
			istanze = text.innerText.match(pattern).length;
		} catch (error) {
			// console.log("NESSUN MATCH");
		}
		
		console.log(istanze);

		let trovato = document.createElement("h2");
		if (istanze) {
			trovato.innerText = `"${termineRicerca}" trovata ${istanze} volte`;
		} else {
			trovato.innerText = `"${termineRicerca}" non trovata`;
		}

		risultato.appendChild(trovato);

		instance.markRegExp(pattern);
	}
	
}

search.addEventListener("input", () => {
	eseguiRicerca(search.value, caseSensitive.checked, wholeWords.checked);
});

search.addEventListener("keyup", (evento) => {
	if (evento.key === "Enter") {
		eseguiRicerca(search.value, caseSensitive.checked, wholeWords.checked);
	}
});

caseSensitive.addEventListener("change", () => {
	eseguiRicerca(search.value, caseSensitive.checked, wholeWords.checked);
});

wholeWords.addEventListener("change", () => {
	eseguiRicerca(search.value, caseSensitive.checked, wholeWords.checked);
});

invio.addEventListener("click", () => {
	eseguiRicerca(search.value, caseSensitive.checked, wholeWords.checked);
});